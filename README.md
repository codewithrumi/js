# JavaScript Live Meetup 


## What is included in the source?
1. HTML template file (app.html)
2. JavaScript source files (files with the (.js) extension included in the js/ folder)

## How to use the source code?
The HTML file (app.html) should be amended to point to the JavaScript source file that you would like to test.

Steps
1. Open the HTML file (app.html)
2. Locate the line that has the ```<script>``` tag
3. edit the source (src) that points to the javascript file
4. Change the path and the file name to reflect the target file.
5. Save the HTML file
6. Open the app.html file in the preferref browser (ex: Chrome)

For example:

If you want to test the JavaScript stored under js/exercise_1/alert.js, you will have to edit the app.html to point to that file as below.

```
<script src="js/exercise_1/alert.js"></script>
```
