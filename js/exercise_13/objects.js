let person = {
    name: "Rumi",
    age: 25,
    interets: ["computing", "travelling", "gadgets"]
}

console.log(person.name + " is " + person.age + " years old")

// iterate through keys of the object to retrieve values
for(key in person){
    console.log(person[key]);
}

// iterate through keys and values of the object
for(const [key, value] of Object.entries(person)){
    console.log(value);
}