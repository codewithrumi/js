
try{
    let x = 5 / 0

    if (!isFinite(x)){
        throw "Division by zero"
    }
}catch(e){
    console.error("This message should appear in the browser console");
    alert("error occured " + e)
}