// Calculate the area of a circle
const PI = 3.1415;
let radius = 20.0;

let circleArea = Math.round(PI * (radius ** 2), 2);

alert(`The area of the circle with radius ${radius} is ${circleArea}`);