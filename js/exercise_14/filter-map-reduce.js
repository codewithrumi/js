/** 
* Calculate the total score of all students with a grade 'A' 
* for both maths and science
*/

let students = [
    {
      reg_id: "001",
      name: "James Smith",
      grade: "A",
      maths: 90,
      science: 86,
    },
    {
      reg_id: "002",
      name: "Sarah Mathew",
      grade: "A",
      maths: 89,
      science: 86,
    },
    {
      reg_id: "003",
      name: "Luke Perry",
      grade: "B",
      maths: 68,
      science: 69,
    },
    {
      reg_id: "004",
      name: "Elizabeth Shaw",
      grade: "B",
      maths: 70,
      science: 69,
    },
    {
      reg_id: "005",
      name: "Mark Taylor",
      grade: "A",
      maths: 85,
      science: 87,
    },
  ];


let totalScore = students.filter(student => {
    return student.grade === "A";
}).map(gradeStudent => {
    return gradeStudent.maths + gradeStudent.science
}).reduce((total, score) => {
    return total + score
},0) 

console.log(totalScore);